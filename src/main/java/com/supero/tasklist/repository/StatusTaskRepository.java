package com.supero.tasklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.supero.tasklist.model.StatusTask;

@Repository
public interface StatusTaskRepository extends JpaRepository<StatusTask, Integer> {
	
}
