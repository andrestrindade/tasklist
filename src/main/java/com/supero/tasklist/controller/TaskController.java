package com.supero.tasklist.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.supero.tasklist.constantes.ConstantesStatusTask;
import com.supero.tasklist.model.StatusTask;
import com.supero.tasklist.model.Task;
import com.supero.tasklist.repository.StatusTaskRepository;
import com.supero.tasklist.repository.TaskRepository;
import com.supero.tasklist.service.TaskService;

@RestController
@RequestMapping("/task")
public class TaskController {
	
	private TaskRepository repository;
	private TaskService service;
	private StatusTaskRepository statusRepository;
	
	TaskController(TaskRepository repository, TaskService service, StatusTaskRepository statusRepository){
		this.repository = repository;
		this.service = service;
		this.statusRepository = statusRepository;
	}
	
	@PostMapping
	public ModelAndView incluirTask(@ModelAttribute Task task, BindingResult bindingResult, Model model) {
		ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("index");
		task.setDataCriacao(new Date());
		Optional<StatusTask> statusEmAndamento = statusRepository.findById(ConstantesStatusTask.STATUS_EM_ANDAMENTO);
		task.setStatus(statusEmAndamento.get());
		repository.save(task);
		model.addAttribute("task", new Task());
		return modelAndView;
	}
	
	@GetMapping("/listar_tasks")
	public ModelAndView listarTasks(Model model) {
		ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("index");
		List<Task> tasks = repository.findAll();
		model.addAttribute("tasks", tasks);
		model.addAttribute("task", new Task());
		return modelAndView;
	}
	
	@GetMapping("/concluirTask")
	public ModelAndView concluirTask(@RequestParam Long codigo, Model model) {
		ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("index");
	    model.addAttribute("task", new Task());
		Task task = repository.findById(codigo).get();
		task.setDataConclusao(new Date());
		Optional<StatusTask> statusConcluido = statusRepository.findById(ConstantesStatusTask.STATUS_CONCLUIDO);
		task.setStatus(statusConcluido.get());
		repository.save(task);
		return modelAndView;
	}

}
