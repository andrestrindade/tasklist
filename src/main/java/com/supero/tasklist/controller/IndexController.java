package com.supero.tasklist.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.supero.tasklist.model.Task;

/**
 * Controller principal para direcionar para página raiz da aplicação
 * @author Andre
 *
 */
@Controller
public class IndexController {
	
	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("task", new Task());
		return "index";
	}

}
